# Documentação GIT

Este documento visa auxiliar no uso das ferramentas do git, tais como o GitLab, git e SourceTree.

## GitLab

- **Projeto**
  - [Criando novo projeto](/Documentos/GitLab/NovoProjeto.md)
  - [Criando um fork](/Documentos/GitLab/NovoFork.md)
     - [Manutenção do fork](/Documentos/GitLab/AlterarFork.md)
- **Permissões**
  - [Criando um novo grupo](/Documentos/GitLab/NovoGrupo.md)

## GIT

- **Instalação**
  - [Windows](Documentos/GIT/InstalarWindows.md)
  - [Linux](Documentos/GIT/InstalarLinux.md)
- **Projeto**
  - [Enviando um projeto existente](Documentos/GIT/EnviarProjeto.md)

*Referências*:

- [Pro Git](https://git-scm.com/book/pt-br/v1)