# Enviar projeto existente

## [Certifique-se de criar um repositório no GitLab antes de prosseguir](Documentos/GitLab/NovoProjeto)

![Passo 1](Imagens/GIT/ProjetoExistente/1.png)

Copie o caminho para o repositório

![Passo 2](Imagens/GIT/ProjetoExistente/2.png)

1. Clique com o botão direito na pasta que deseja enviar
1. Clique em *Git Bash Here*

![Passo 3](Imagens/GIT/ProjetoExistente/3.png)

Digite os seguintes comandos:

```sh
git init #Para inicializar o repositório local
git remote add origin <caminho para o repositório> #Para ligar o repositório local ao remoto
git add . #Para adicionar todos os arquivos da pasta a área para commit
git commit -m "<Mensagem do commit>" #Para consolidar um commit
git push -u origin master #Para enviar o novo ramo ao projeto vazio
```

![Passo 4](Imagens/GIT/ProjetoExistente/4.png)

O repositório deverá aparecer sincronizado.