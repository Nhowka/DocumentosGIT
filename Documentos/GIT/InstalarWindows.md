# Instalação do GIT for Windows

## [Link para download](https://git-for-windows.github.io/)

### Passos para instalação do GIT for Windows

![Passo 1](Imagens/GIT/Windows/1.png)

1. Faça o download do arquivo de instalação

![Passo 2](Imagens/GIT/Windows/2.png)

1. Clique em *Next*.

![Passo 3](Imagens/GIT/Windows/3.png)

1. Aceite as configurações padrão para criar menus de contexto git, integração dos arquivos de configuração com o editor de texto padrão e arquivos *.sh* com o Bash.
1. Clique em *Next*.

![Passo 4](Imagens/GIT/Windows/4.png)

1. Selecione *Use Git from the Windows Command Prompt* para integrar o git com o Prompt de comando.
1. Clique em *Next*.

![Passo 5](Imagens/GIT/Windows/5.png)

1. Aplicativo padrão para conexões SSH.
1. Caso haja um cliente SSH compatível com PuTTY, selecione esta opção. O caminho deverá ser detectado automaticamente.
1. Clique em *Next*.

![Passo 6](Imagens/GIT/Windows/6.png)

1. Para um melhor controle de versão em ambientes multiplataforma, selecione *Checkout Windows-style, commit Unix-style line endings* para padronizar as diferenças de quebras de linha dos dois sistemas.
1. Clique em *Next*.

![Passo 7](Imagens/GIT/Windows/7.png)

1. Use MinTTY para uma melhor experiência usando o Git Bash.
1. Clique em *Next*.

![Passo 8](Imagens/GIT/Windows/8.png)

1. Para uma melhor performance, mantenha *Enable file system caching* para um melhor acesso ao disco.
1. Mantenha *Enable Git Credential Manager* ativo para uma melhor experiência em servidores Git que necessitam de autenticação.
1. Pelo risco de loops acidentais, recomenda-se manter *Enable symbolics links* desativado.
1. Clique em *Next*.

![Passo 9](Imagens/GIT/Windows/9.png)

1. Devido ao pouco teste da ferramenta, recomenda-se manter *Enable experimental, builtin difftool* desativado até que obtenha uma maior maturidade.
1. Clique em *Install*

![Passo 10](Imagens/GIT/Windows/10.png)

1. Após a instalação pode-se desmarcar a opção *View Release Notes*
1. Clique em *Finish* para terminar