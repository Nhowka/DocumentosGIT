# Instalação do GIT no Linux

## Passos para instalação do GIT

- [Sistemas baseados em Debian](#debian)
- [Sistemas baseados em CentOS](#centos)
- [Outros sistemas](#outros)

Em um terminal digite:

## Debian

```sh
sudo apt install git-all
```

## CentOS

```sh
sudo yum install git-all
```

**Caso necessário digite sua senha para acesso root.**


## Outros

Informações para instalação em outros sistemas Linux pode ser encontrada [AQUI](https://git-scm.com/download/linux).