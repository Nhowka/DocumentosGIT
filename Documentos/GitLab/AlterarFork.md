# Manutenção do fork

## Passos comuns após criar um novo fork

### Certifique-se estar no projeto que deseja modificar

![Passo 1](Imagens/GitLab/ConfigurarProjeto/1.png)

1. Clique na engrenagem no canto direito do menu de projeto.
1. Clique em "Edit Project".

![Project settings](Imagens/GitLab/ConfigurarProjeto/2.png)

- Em *Project settings* você pode:
1. Renomear o projeto.
1. Alterar a descrição do projeto.

![Rename repository](Imagens/GitLab/ConfigurarProjeto/3.png)

- Em *Rename repository* você pode:
1. Renomear o projeto.
1. Alterar o caminho do projeto.

![Transfer project](Imagens/GitLab/ConfigurarProjeto/4.png)

- Em *Transfer repository* você pode alterar o dono do projeto.
1. Selecione o novo dono do projeto.
1. Clique em "Transfer project".

![Remove fork relationship](Imagens/GitLab/ConfigurarProjeto/5.png)

- Em *Transfer repository* você pode remover a relação de fork mantendo apenas o histórico do projeto original.
1. Clique em "Remove fork relationship".

## Ações potencialmente destrutivas requerem confirmação

![Confirmação](Imagens/GitLab/ConfigurarProjeto/6.png)

1. Digite o nome do projeto no campo.
1. Clique em "Confirm".
