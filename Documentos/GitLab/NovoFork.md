# Criando um novo fork

## Passos para criação de um novo fork

### Certifique-se estar no projeto que deseja criar um fork

![Passo 1](Imagens/GitLab/NovoProjetoFork/5.png)

1. Clique no botão "Fork".

![Passo 2](Imagens/GitLab/NovoProjetoFork/6.png)

1. Escolha o local do novo fork.
   - Neste passo pode ser escolhido um grupo ou usuário como dono do fork.

![Passo 3](Imagens/GitLab/NovoProjetoFork/7.png)

- Fork é uma cópia do projeto original, juntamente com todo o histórico e branches.