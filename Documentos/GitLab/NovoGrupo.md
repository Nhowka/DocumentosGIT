# Criando um novo grupo

## Passos para criação de um novo grupo

### Certifique-se estar na seção de grupos

![Passo 1](Imagens/GitLab/Grupos/1.png)

1. Clique no botão no canto superior esquerdo para abrir o menu
1. Selecione "Groups"

![Passo 2](Imagens/GitLab/Grupos/2.png)

1. Clique em "New Group"

![Passo 3](Imagens/GitLab/Grupos/3.png)

1. Escolha um nome para o grupo.
1. Escolha uma descrição para o grupo.
1. Escolha uma imagem representativa para o grupo.
1. Escolha a visibilidade do grupo.
   - Private: Apenas membros do grupo podem ter acesso ao grupo e seus projetos.
   - Internal: Usuários com um login ativo podem visualizar o grupo e seus projeto.
   - Public: Usuários podem visualizar o grupo e seus projeto independente de login.
1. Clique em "Create group".