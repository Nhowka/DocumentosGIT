# Criando um novo projeto

## Passos para criação de um novo projeto

### Certifique-se estar na seção de projetos

![Passo 1](Imagens/GitLab/NovoProjetoFork/1.png)

1. Clique no botão no canto superior esquerdo para abrir o menu
1. Selecione "Projects"

![Passo 2](Imagens/GitLab/NovoProjetoFork/2.png)

1. Clique em "New Project"

![Passo 3](Imagens/GitLab/NovoProjetoFork/3.png)

1. Selecione onde o projeto vai ser criado.
   - Pode ser escolhido tanto um usuário como um grupo como dono do projeto.
1. Escolha um nome para o projeto.
1. Escolha uma descrição para o projeto. (Opcional)
1. Escolha as opções de visibilidade do projeto.
   - Private: Apenas usuários e grupos explicitamente permitidos podem ter acesso ao projeto.
   - Internal: Usuários com um login ativo podem visualizar o projeto.
   - Public: Usuários podem visualizar o projeto independente de login.
1. Clique em "Create project" para finalizar.